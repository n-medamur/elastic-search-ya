import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        archiveAddedPlace: JSON.parse(localStorage.getItem('archiveAddedPlace') || '[]')
    },
    mutations: {
        addLocalStorage(state, data) {
            state.archiveAddedPlace.push(data)
            localStorage.setItem('archiveAddedPlace', JSON.stringify(state.archiveAddedPlace))
        }
    },
    actions: {
        addLocalStorage({commit, state}, data) {
            const isFind = state.archiveAddedPlace.findIndex(el => el.displayName === data.displayName)
            isFind !== 0 ? commit('addLocalStorage', data) : ''
        }
    }
})
